package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestUsefulness {

    @Test
    public void testDesc() {
        // Create an instance of the Usefulness class
        Usefulness usefulness = new Usefulness();

        // Call the "desc" method
        String result = usefulness.desc();

        // Define the expected result
        String expected = "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.";

        // Assert that the actual result matches the expected result
        assertEquals(expected, result);
    }

    @Test
    public void testFunctionWF() {
        // Create an instance of the Usefulness class
        Usefulness usefulness = new Usefulness();

        // Call the "functionWF" method (no assertion needed, just checking for any exceptions)
        usefulness.functionWF();
    }
}
