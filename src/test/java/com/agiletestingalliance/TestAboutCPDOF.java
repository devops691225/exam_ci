package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestAboutCPDOF {

    @Test
    public void testDesc() {
        // Create an instance of the AboutCPDOF class
        AboutCPDOF aboutCPDOF = new AboutCPDOF();

        // Call the "desc" method
        String result = aboutCPDOF.desc();

        // Define the expected result
        String expected = "CP-DOF certification program covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: <br> 1. Completely hands on. <br> 2. 100% Lab/Tools Driven <br> 3. Covers all the tools in entire lifecycle <br> 4. You will not only learn but experience the entire DevOps lifecycle. <br> 5. Practical Assessment to help you solidify your learnings.";

        // Assert that the actual result matches the expected result
        assertEquals(expected, result);
    }
}