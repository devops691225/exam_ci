package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestDuration {

    @Test
    public void testDur() {
        // Create an instance of the Duration class
        Duration duration = new Duration();

        // Call the "dur" method
        String result = duration.dur();

        // Define the expected result
        String expected = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";

        // Assert that the actual result matches the expected result
        assertEquals(expected, result);
    }

    @Test
    public void testCalculateIntValue() {
        // Create an instance of the Duration class
        Duration duration = new Duration();

        // Call the "calculateIntValue" method (no assertion needed, just checking for any exceptions)
        duration.calculateIntValue();
    }
}
